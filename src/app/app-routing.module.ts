import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FriendSuggestionsComponent } from './components/friend-suggestions/friend-suggestions.component';
import { FriendsPageComponent } from './components/friends-page/friends-page.component';
import { GroupsComponent } from './components/groups/groups.component';
import { NewsPageComponent } from './components/news-page/news-page.component';
import { SettingComponent } from './components/setting/setting.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'setting',
    component: SettingComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'news',
    component: NewsPageComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'groups',
    component: GroupsComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'friends',
    component: FriendsPageComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
