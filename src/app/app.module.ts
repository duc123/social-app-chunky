import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AppRoutingModule } from './app-routing.module';
import { ButtonModule } from 'primeng/button';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { TooltipModule } from 'primeng/tooltip';
import { FormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { MessagesModule } from 'primeng/messages';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MessageModule } from 'primeng/message';
import { HttpClientModule } from '@angular/common/http';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { AvatarModule } from 'primeng/avatar';
import { SkeletonModule } from 'primeng/skeleton';
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';

import { environment } from 'src/environments/environment';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LoginEffect } from 'src/stores/login/effect';
import loginReducer from 'src/stores/login/reducer';
import registerReducer from 'src/stores/register/reducer';
import { RegisterEffect } from 'src/stores/register/effect';
import { MessageService } from 'primeng/api';
import { HeaderComponent } from './components/header/header.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { NavSideBarComponent } from './components/nav-side-bar/nav-side-bar.component';
import { PostsComponent } from './components/posts/posts.component';
import { NewsComponent } from './components/news/news.component';
import { FriendSuggestionsComponent } from './components/friend-suggestions/friend-suggestions.component';
import { NewsItemComponent } from './components/news-item/news-item.component';
import authReducer from 'src/stores/auth/reducer';
import { AuthEffect } from 'src/stores/auth/effect';
import { InfoSmallBlockComponent } from './components/info-small-block/info-small-block.component';
import { MobileNavBarComponent } from './components/mobile-nav-bar/mobile-nav-bar.component';
import { SettingComponent } from './components/setting/setting.component';
import { GroupsComponent } from './components/groups/groups.component';
import { NewsPageComponent } from './components/news-page/news-page.component';
import { FriendsPageComponent } from './components/friends-page/friends-page.component';
import { CreatePostComponent } from './components/create-post/create-post.component';
import { CalendarModule } from 'primeng/calendar';
import { AlertsComponent } from './components/alerts/alerts.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    SearchBarComponent,
    NavSideBarComponent,
    PostsComponent,
    NewsComponent,
    FriendSuggestionsComponent,
    NewsItemComponent,
    InfoSmallBlockComponent,
    MobileNavBarComponent,
    SettingComponent,
    GroupsComponent,
    NewsPageComponent,
    FriendsPageComponent,
    CreatePostComponent,
    AlertsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    ButtonModule,
    StoreModule.forRoot(
      { login: loginReducer, register: registerReducer, auth: authReducer },
      {}
    ),
    EffectsModule.forRoot([LoginEffect, RegisterEffect, AuthEffect]),
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    TooltipModule,
    FormsModule,
    CheckboxModule,
    MessageModule,
    MessagesModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ConfirmDialogModule,
    AvatarModule,
    SkeletonModule,
    DialogModule,
    InputTextareaModule,
    DropdownModule,
    CalendarModule,
  ],
  providers: [MessageService, ConfirmationService],
  bootstrap: [AppComponent],
})
export class AppModule {}
