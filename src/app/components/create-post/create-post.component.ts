import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { FileService } from 'src/app/services/file.service';
import { PostService } from 'src/app/services/post.service';
import { userInterface } from 'src/interfaces/user.interface';
import { authState } from 'src/stores/auth/state';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss'],
})
export class CreatePostComponent implements OnInit, OnDestroy {
  authInfo$: any;
  authInfo: any;
  postModal$ = this.postService.postModalVisible$;
  postModal: boolean = false;

  images: any[] = [];

  form: FormGroup;

  showAddPost() {
    this.postService.showPostModal();
  }

  get disbaled() {
    return this.form.invalid;
  }

  async onChange(event: any) {
    const files = event.target.files;
    const res = await this.fileService.fileListToBase64(files);
    if (res.length > 0) {
      this.images = [...res];
    } else {
      this.images = [];
    }
  }

  constructor(
    private store: Store<{ auth: authState }>,
    private postService: PostService,
    private fb: FormBuilder,
    private fileService: FileService
  ) {
    this.authInfo$ = this.store.select('auth').subscribe(res => {
      this.authInfo = res.user;
    });
    this.postModal$.subscribe(el => (this.postModal = el));

    this.form = this.fb.group({
      content: ['', [Validators.required]],
      files: [null],
    });
  }

  ngOnInit(): void {}
  ngOnDestroy(): void {
    this.authInfo$.unsubscribe();
  }
}
