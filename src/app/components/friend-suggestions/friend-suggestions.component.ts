import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserService } from 'src/app/service/user.service';
import { authState } from 'src/stores/auth/state';
import { loginState } from 'src/stores/login/state';

@Component({
  selector: 'app-friend-suggestions',
  templateUrl: './friend-suggestions.component.html',
  styleUrls: ['./friend-suggestions.component.scss'],
})
export class FriendSuggestionsComponent implements OnInit {
  suggestions$ = this.userService.getAllUsers();
  authInfo: any;

  onAdd(subjectId: string) {
    this.userService.add(this.authInfo.id, subjectId).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }

  onAccept(subjectId: string) {
    this.userService.accept(this.authInfo.id, subjectId);
  }

  onCancel(subjectId: string) {
    this.userService.cancel(this.authInfo.id, subjectId);
  }

  onDecline(subjectId: string) {
    this.userService.decline(this.authInfo.id, subjectId);
  }

  checkStatus(id: string): string {
    if (this.authInfo.friends.includes(id)) return 'friend';
    if (this.authInfo.invitations.includes(id)) return 'accept';
    if (this.authInfo.addList.includes(id)) return 'pending';

    return 'no';
  }

  constructor(
    private userService: UserService,
    private store: Store<{ auth: authState }>
  ) {
    this.store.select('auth').subscribe(el => {
      this.authInfo = el.user;
    });
  }

  ngOnInit(): void {}
}
