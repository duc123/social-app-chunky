import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ConfirmationService } from 'primeng/api';
import { getUserInfoRequest, resetUserInfo } from 'src/stores/auth/actions';
import { authState } from 'src/stores/auth/state';
import { LogoutRequest } from 'src/stores/login/actions';
import { selectLoginUid } from 'src/stores/login/selector';
import { loginState } from 'src/stores/login/state';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  storeSub: any;
  uid: string;

  onLogout() {
    this.confirmService.confirm({
      message: 'Are you sure you want to logout?',
      accept: () => {
        this.store.dispatch(new LogoutRequest());
      },
    });
  }

  constructor(
    private store: Store<{ login: loginState; auth: authState }>,
    private confirmService: ConfirmationService,
    private router: Router
  ) {
    this.storeSub = this.store.select(selectLoginUid).subscribe(res => {
      this.uid = res;
      if (res) {
        this.store.dispatch(getUserInfoRequest({ payload: res }));
      } else {
        this.store.dispatch(resetUserInfo());
        this.router.navigate(['login']);
      }
    });
    // this.storeSub = this.store.select('login').subscribe(values => {
    // });
    // this.store.select('auth').subscribe(res => {});
  }

  ngOnInit(): void {}
  ngOnDestroy(): void {}
}
