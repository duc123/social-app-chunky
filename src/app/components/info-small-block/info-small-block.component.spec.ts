import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoSmallBlockComponent } from './info-small-block.component';

describe('InfoSmallBlockComponent', () => {
  let component: InfoSmallBlockComponent;
  let fixture: ComponentFixture<InfoSmallBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InfoSmallBlockComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(InfoSmallBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
