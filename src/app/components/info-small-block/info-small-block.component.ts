import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { authState } from 'src/stores/auth/state';

@Component({
  selector: 'app-info-small-block',
  templateUrl: './info-small-block.component.html',
  styleUrls: ['./info-small-block.component.scss'],
})
export class InfoSmallBlockComponent implements OnInit, OnDestroy {
  authInfo: any;
  loading: boolean = false;
  store$: any;
  constructor(private store: Store<{ auth: authState }>) {
    this.store$ = this.store.select('auth').subscribe(values => {
      this.authInfo = values.user;

      this.loading = values.loading;
      // console.log(this.loading);
    });
  }

  ngOnDestroy(): void {
    this.store$.unsubscribe();
  }
  ngOnInit(): void {}
}
