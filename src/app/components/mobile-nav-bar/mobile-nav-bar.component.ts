import { Component, OnInit } from '@angular/core';
import { appNavList } from 'src/app/constants/nav.list';

@Component({
  selector: 'app-mobile-nav-bar',
  templateUrl: './mobile-nav-bar.component.html',
  styleUrls: ['./mobile-nav-bar.component.scss'],
})
export class MobileNavBarComponent implements OnInit {
  navList: any[] = appNavList;

  constructor() {}

  ngOnInit(): void {}
}
