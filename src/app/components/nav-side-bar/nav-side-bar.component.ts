import { Component, OnInit } from '@angular/core';
import { appNavList } from 'src/app/constants/nav.list';

@Component({
  selector: 'app-nav-side-bar',
  templateUrl: './nav-side-bar.component.html',
  styleUrls: ['./nav-side-bar.component.scss'],
})
export class NavSideBarComponent implements OnInit {
  navList: any[] = appNavList;

  constructor() {}

  ngOnInit(): void {}
}
