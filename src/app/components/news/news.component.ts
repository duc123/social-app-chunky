import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {
  news: any[] = [];

  getNewsList() {
    this.newsService.getNews().subscribe((el: any) => {
      this.news = el.articles;
    });
  }

  constructor(private newsService: NewsService) {}

  ngOnInit(): void {
    this.getNewsList();
  }
}
