import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { FileService } from 'src/app/services/file.service';
import { UtilService } from 'src/app/services/util.service';
import { Gender } from 'src/interfaces/gender.interface';
import { Nation } from 'src/interfaces/nation.interface';
import { editInfoRequest } from 'src/stores/auth/actions';
import { selectAuth, selectLoading } from 'src/stores/auth/selector';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent implements OnInit, OnDestroy {
  form: FormGroup;
  genders: Gender[] = [
    {
      name: 'Male',
      code: 'male',
    },
    {
      name: 'Female',
      code: 'female',
    },
  ];
  nations: Nation[] = [];
  nations$: any;
  auth$: any;
  authInfo: any;

  avatar: any = '';

  loading: boolean = false;

  submit() {
    this.store.dispatch(
      editInfoRequest({
        payload: { ...this.form.value, avatar: this.authInfo.avatar },
      })
    );
  }

  get username() {
    return this.form.controls['username'];
  }
  get firstname() {
    return this.form.controls['firstname'];
  }
  get lastname() {
    return this.form.controls['lastname'];
  }
  get gender() {
    return this.form.controls['gender'];
  }
  get dateOfBirth() {
    return this.form.controls['dateOfBirth'];
  }
  get nationality() {
    return this.form.controls['nationality'];
  }

  get disabled() {
    return this.form.invalid || this.loading;
  }

  async onChange(e: any) {
    const files = e.target.files;
    if (files.length > 0) {
      this.form.patchValue({ fileSource: files[0] });

      const base64 = await this.fileService.fileListToBase64(files);
      this.avatar = base64;
    } else {
    }
  }

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private store: Store,
    private fileService: FileService
  ) {
    this.form = this.fb.group({
      email: [{ value: '', disabled: true }],
      username: ['', [Validators.required]],
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      nationality: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      occupation: [''],
      about: [''],
      file: [''],
      fileSource: [null],
      dateOfBirth: ['', [Validators.required]],
      avatar: [''],
    });

    this.store.select(selectLoading).subscribe(res => {
      this.loading = res;
    });

    this.store.select(selectAuth).subscribe(res => {
      this.authInfo = res;

      if (this.authInfo) {
        const {
          email,
          username,
          firstname,
          lastname,
          nationality,
          gender,
          occupation,
          about,
          dateOfBirth: date,
          avatar: url,
        } = this.authInfo;
        if (url) this.avatar = url;
        this.form.patchValue({
          email: email,
          username,
          firstname,
          lastname,
          nationality,
          gender,
          occupation,
          about,
          dateOfBirth: date ? date.toDate() : '',
        });
      }
    });
  }

  ngOnInit(): void {
    this.utilService.getNations();
    this.nations$ = this.utilService.nations$.subscribe(el => {
      this.nations = el;
    });
  }

  ngOnDestroy(): void {
    this.nations$.unsubscribe();
  }
}
