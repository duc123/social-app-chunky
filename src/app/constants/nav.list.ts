export const appNavList: any[] = [
  {
    title: 'Home',
    url: 'home',
    icon: 'pi pi-home',
  },
  {
    title: 'Setting',
    url: 'setting',
    icon: 'pi pi-cog',
  },
  {
    title: 'News',
    url: 'news',
    icon: 'pi pi-globe',
  },
  {
    title: 'Groups',
    url: 'groups',
    icon: 'pi pi-users',
  },
  {
    title: 'Friends',
    url: 'friends',
    icon: 'pi pi-user-plus',
  },
];
