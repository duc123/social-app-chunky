import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { loginState } from 'src/stores/login/state';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  uid: string = '';
  constructor(
    private store: Store<{ login: loginState }>,
    private router: Router
  ) {
    this.store.select('login').subscribe(values => {
      this.uid = values.uid;
    });
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.uid) {
      return true;
    }
    this.router.navigate(['login']);
    return false;
  }
}
