/* eslint-disable @ngrx/prefer-action-creator-in-dispatch */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { LoginService } from 'src/app/services/login.service';
import { LoginRequest } from 'src/stores/login/actions';
import { selectLoginLoading, selectLoginUid } from 'src/stores/login/selector';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  form: FormGroup;
  passwordType: string = 'password';
  loading: boolean = false;
  errors: any[] = [];

  loading$: any;
  uid$: any;

  onChangePasswordType() {
    this.passwordType = this.passwordType === 'password' ? 'text' : 'password';
  }

  get email() {
    return this.form.controls['email'];
  }

  get password() {
    return this.form.controls['password'];
  }

  get disabled() {
    return this.form.invalid;
  }

  get isLoading() {
    return this.loading;
  }

  getEmailMsg() {
    if (this.email.errors && this.email.touched) {
      if (this.email.errors['required']) return 'This field can not be blank';
      if (this.email.errors['email']) return 'Invalid email';
    }

    return '';
  }

  getPasswordMsg() {
    if (this.password.errors && this.password.touched) {
      if (this.password.errors['required'])
        return 'This field can not be blank';
      if (this.password.errors['minlength'])
        return 'Password must be longer than 6 characters';
    }

    return '';
  }

  submit() {
    const { email, password } = this.form.value;

    this.store.dispatch(new LoginRequest({ email, password }));
  }

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private router: Router,
    private loginService: LoginService
  ) {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
    this.uid$ = this.store.select(selectLoginUid).subscribe(res => {
      if (res) {
        this.router.navigate(['']);
      }
    });
    this.loading$ = this.store.select(selectLoginLoading).subscribe(res => {
      console.log(res);

      this.loading = res;
    });
    this.loginService.errors$.subscribe(el => {
      this.errors = el;
    });
  }

  ngOnInit(): void {}
  ngOnDestroy(): void {
    this.loginService.clearMsg();
    this.loading$.unsubscribe();
    this.uid$.unsubscribe();
  }
}
