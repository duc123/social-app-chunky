/* eslint-disable @ngrx/prefer-action-creator-in-dispatch */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { RegisterService } from 'src/app/services/register.service';
import { registerData } from 'src/interfaces/register.data';
import { loginState } from 'src/stores/login/state';
import { registerRequest } from 'src/stores/register/actions';
import { registerState } from 'src/stores/register/state';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  form: FormGroup;
  passwordType: string = 'password';
  storeSub: any;
  storeLoginSub: any;
  loading: boolean = false;
  errors: any[] = [];

  onChangePasswordType() {
    this.passwordType = this.passwordType === 'password' ? 'text' : 'password';
  }

  get username() {
    return this.form.controls['username'];
  }

  get email() {
    return this.form.controls['email'];
  }

  get password() {
    return this.form.controls['password'];
  }

  get accept() {
    return this.form.controls['accept'];
  }

  get disabled() {
    return this.form.invalid;
  }

  get isLoading() {
    return this.loading;
  }

  getUsernameMsg() {
    if (this.username.errors && this.username.touched) {
      if (this.username.errors['required'])
        return 'This field can not be blank';
    }

    return '';
  }

  getEmailMsg() {
    if (this.email.errors && this.email.touched) {
      if (this.email.errors['required']) return 'This field can not be blank';
      if (this.email.errors['email']) return 'Invalid email';
    }

    return '';
  }

  getPasswordMsg() {
    if (this.password.errors && this.password.touched) {
      if (this.password.errors['required'])
        return 'This field can not be blank';
      if (this.password.errors['minLength'])
        return 'Password must be longer than 6 characters';
    }

    return '';
  }

  getAcceptMsg() {
    if (this.accept.errors && this.accept.touched) {
      return 'Please accept terms and conditions';
    }
    return '';
  }

  submit() {
    const { username, email, password } = this.form.value;
    const data: registerData = { username, email, password };
    this.store.dispatch(registerRequest({ payload: data }));
  }

  constructor(
    private fb: FormBuilder,
    private store: Store<{ register: registerState; login: loginState }>,
    private router: Router,
    private registerService: RegisterService
  ) {
    this.form = this.fb.group({
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      accept: [false, Validators.requiredTrue],
    });
    this.storeSub = this.store.select('register').subscribe(res => {
      this.loading = res.loading;
    });

    this.storeLoginSub = this.store.select('login').subscribe(res => {
      if (res.uid) this.router.navigate(['']);
    });

    this.registerService.errors$.subscribe(value => (this.errors = value));
  }

  ngOnInit(): void {}
  ngOnDestroy(): void {
    this.storeSub.unsubscribe();
    this.storeLoginSub.unsubscribe();
    this.registerService.clearMsg();
  }
}
