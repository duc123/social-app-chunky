import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { from, map, Observable } from 'rxjs';
import { arrayRemove, arrayUnion } from 'firebase/firestore';
import { Store } from '@ngrx/store';
import { loginState } from 'src/stores/login/state';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { authInfo } from 'src/interfaces/authInfo.interface';
import { FileService } from '../services/file.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  uid: string;

  updateAccountInfo(data: authInfo): Observable<any> {
    const {
      username,
      firstname,
      lastname,
      nationality,
      gender,
      occupation,
      about,
      dateOfBirth,
      fileSource,
      avatar,
    } = data;

    if (!fileSource) {
      return from(
        this.afs.collection('users').doc(this.uid).update({
          username,
          firstname,
          lastname,
          nationality,
          gender,
          occupation,
          about,
          dateOfBirth,
        })
      );
    } else {
      return from(
        this.fileService
          .processStorageImageUpload(fileSource, avatar)
          .then(url => {
            this.afs.collection('users').doc(this.uid).update({
              username,
              firstname,
              lastname,
              nationality,
              gender,
              occupation,
              about,
              dateOfBirth,
              avatar: url,
            });
          })
      );
    }
  }

  getUser(uid: string): Observable<any> {
    return this.afs
      .collection('users')
      .doc(uid)
      .snapshotChanges()
      .pipe(
        map(snap => {
          return snap.payload.data();
        })
      );
  }

  getAllUsers(): Observable<any> {
    return this.afs
      .collection('users')
      .snapshotChanges()
      .pipe(
        map(snaps => {
          return snaps
            .map(snap => {
              return snap.payload.doc.data();
            })
            .filter((el: any) => el?.id !== this.uid);
        })
      );
  }

  add(uid: string, subjectId: string): Observable<any> {
    return from(
      this.afs
        .collection('users')
        .doc(uid)
        .update({
          addList: arrayUnion(subjectId),
        })
        .then(() => {
          this.afs
            .collection('users')
            .doc(subjectId)
            .update({
              invitations: arrayUnion(uid),
            });
        })
    );
  }

  accept(uid: string, subjectId: string): Observable<any> {
    return from(
      this.afs
        .collection('users')
        .doc(uid)
        .update({
          invitations: arrayRemove(subjectId),
          friends: arrayUnion(subjectId),
        })
        .then(() => {
          this.afs
            .collection('users')
            .doc(subjectId)
            .update({
              addList: arrayRemove(uid),
              friends: arrayUnion(uid),
            });
        })
    );
  }

  cancel(uid: string, subjectId: string): Observable<any> {
    return from(
      this.afs
        .collection('users')
        .doc(uid)
        .update({
          addList: arrayRemove(subjectId),
        })
        .then(() => {
          this.afs
            .collection('users')
            .doc(subjectId)
            .update({
              invitations: arrayRemove(uid),
            });
        })
    );
  }

  decline(uid: string, subjectId: string): Observable<any> {
    return from(
      this.afs
        .collection('users')
        .doc(uid)
        .update({
          invitations: arrayRemove(subjectId),
        })
        .then(() => {
          this.afs
            .collection('users')
            .doc(subjectId)
            .update({
              addList: arrayRemove(uid),
            });
        })
    );
  }

  unfriend(uid: string, subjectId: string): Observable<any> {
    return from(
      this.afs
        .collection('users')
        .doc(uid)
        .update({
          friends: arrayRemove(subjectId),
        })
        .then(() => {
          this.afs
            .collection('users')
            .doc(subjectId)
            .update({
              friends: arrayRemove(uid),
            });
        })
    );
  }

  constructor(
    private afs: AngularFirestore,
    private store: Store<{ login: loginState }>,
    private fileService: FileService
  ) {
    store.select('login').subscribe(res => (this.uid = res.uid));
  }
}
