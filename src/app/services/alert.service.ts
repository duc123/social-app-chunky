import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { BehaviorSubject } from 'rxjs';
import { messageInterface } from 'src/interfaces/message.interface';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  alerts: any[] = [];
  alerts$: BehaviorSubject<messageInterface[]> = new BehaviorSubject(
    this.alerts
  );

  addAlert(alert: messageInterface) {
    this.messageService.add(alert);
  }

  removeAlert() {
    this.alerts$.next([]);
  }

  constructor(private messageService: MessageService) {}
}
