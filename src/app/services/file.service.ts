import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FileService {
  deleteFile(url: string) {
    return this.afsr.storage.refFromURL(url).delete();
  }

  processStorageImageUpload(file: File, oldAvatar: any) {
    return new Promise((resolve, reject) => {
      const name = file.name;
      const date = new Date().toISOString();
      const fileName = `/avatar/${name} + ${date}`;
      const storageRef = this.afsr.ref(fileName);
      const contentType = { contentType: 'image/jpeg' };
      const task = this.afsr.upload(fileName, file, contentType);
      task
        .snapshotChanges()
        .pipe(
          finalize(() => {
            storageRef.getDownloadURL().subscribe(url => {
              if (oldAvatar) {
                this.deleteFile(oldAvatar)
                  .then(() => {
                    resolve(url);
                  })
                  .catch(err => {
                    console.log(err);
                  });
              } else {
                resolve(url);
              }
            });
          })
        )
        .subscribe();
    });
  }

  async fileListToBase64(fileList: FileList) {
    // create function which return resolved promise
    // with data:base64 string
    function getBase64(file: File) {
      const reader = new FileReader();
      return new Promise(resolve => {
        reader.onload = (ev: any) => {
          resolve(ev.target.result);
        };
        reader.readAsDataURL(file);
      });
    }
    const promises = [];

    // loop through fileList with for loop
    for (let i = 0; i < fileList.length; i++) {
      promises.push(getBase64(fileList[i]));
    }

    return await Promise.all(promises);
  }

  constructor(private afsr: AngularFireStorage) {}
}
