import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { BehaviorSubject, from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private afAuth: AngularFireAuth) {}

  errors: any[] = [];

  errors$: BehaviorSubject<any[]> = new BehaviorSubject(this.errors);

  addMsg(item: any) {
    this.errors$.next([item]);
  }

  clearMsg() {
    this.errors$.next([]);
  }

  login(email: string, password: string): Observable<any> {
    return from(this.afAuth.signInWithEmailAndPassword(email, password));
  }
  logout(): Observable<any> {
    return from(this.afAuth.signOut());
  }
}
