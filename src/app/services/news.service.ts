import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  private key: string = 'b66c172963834ea7a0134eb41d658ec9';
  private url: string = 'https://newsapi.org/v2';
  private type: string = 'top-headlines';

  getNews(): Observable<any> {
    return this.http.get(
      `${this.url}/${this.type}?country=us&pageSize=10&apiKey=${this.key}`
    );
  }

  constructor(private http: HttpClient) {}
}
