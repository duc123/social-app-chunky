import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  postModalVisible: boolean = false;
  postModalVisible$: BehaviorSubject<boolean> = new BehaviorSubject(
    this.postModalVisible
  );

  showPostModal() {
    this.postModalVisible$.next(true);
  }
  hidePostModal() {
    this.postModalVisible$.next(false);
  }

  addPost() {}

  getPostNewsFeed() {}

  constructor(private afs: AngularFirestore) {}
}
