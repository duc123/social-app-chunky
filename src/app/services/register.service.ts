import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { registerData } from 'src/interfaces/register.data';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  errors: any[] = [];

  errors$: BehaviorSubject<any[]> = new BehaviorSubject(this.errors);

  addMsg(item: any) {
    this.errors$.next([item]);
  }

  clearMsg() {
    this.errors$.next([]);
  }

  register(data: registerData): Observable<any> {
    const { email, password } = data;
    return from(this.afAuth.createUserWithEmailAndPassword(email, password));
  }

  registerSetData(
    uid: string,
    username: string,
    email: string
  ): Observable<any> {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `/users/${uid}`
    );
    const user = {
      id: uid,
      username,
      email,
      avatar: '',
      about: '',
      nationality: '',
      dateOfBirth: '',
      occupation: '',
      firstname: '',
      lastname: '',
      friends: [],
      invitations: [],
      addList: [],
    };
    return from(userRef.set(user, { merge: true }));
  }

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth) {}
}
