import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Nation } from 'src/interfaces/nation.interface';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  url: string = 'https://restcountries.com/v3.1/all';
  nations: Nation[] = [];
  nations$: BehaviorSubject<Nation[]> = new BehaviorSubject(this.nations);

  getNations() {
    this.http.get(this.url).subscribe((res: any) => {
      const arr = res.map((el: any) => ({
        name: el.name.common,
        code: el.name.common,
        flag: el.flag,
      }));
      this.nations$.next(arr);
    });
  }

  constructor(private http: HttpClient) {}
}
