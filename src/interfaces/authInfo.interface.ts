export interface authInfo {
  username: string;
  firstname: string;
  lastname: string;
  gender: string;
  dateOfBirth: string;
  nationality: string;
  occupation: string;
  about: string;
  fileSource: File;
  avatar?: string;
}
