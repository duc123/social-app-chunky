export interface messageInterface {
  severity: string;
  summary: string;
  detail: string;
}
