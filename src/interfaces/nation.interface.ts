export interface Nation {
  name: string;
  code: string;
  flag: string;
}
