export interface userInterface {
  username: string;
  email: string;
  avatar: string;
  about: string;
  nationality: string;
  dateOfBirth: string;
  occupation: string;
  firstname: string;
  lastname: string;
  id: string;
}
