import { createAction, props } from '@ngrx/store';
import { authInfo } from 'src/interfaces/authInfo.interface';
import { userInterface } from 'src/interfaces/user.interface';

export enum AuthActionTypes {
  GET_USER_INFO_REQUEST = '[AUTH] get user info request',
  GET_USER_INFO_SUCCESS = '[AUTH] get user info success',
  GET_USER_INFO_FAILURE = '[AUTH] get user info failure',
  RESET_USER_INFO = '[AUTH] reset user info',

  EDIT_INFO_REQUEST = '[AUTH] EDIT INFO REQUEST',
  EDIT_INFO_SUCCESS = '[AUTH] EDIT INFO SUCCESS',
  EDIT_INFO_FAILURE = '[AUTH] EDIT INFO FAILURE',
}

export const getUserInfoRequest = createAction(
  AuthActionTypes.GET_USER_INFO_REQUEST,
  props<{ payload: string }>()
);

export const getUserInfoRequestSuccess = createAction(
  AuthActionTypes.GET_USER_INFO_SUCCESS,
  props<{ payload: userInterface }>()
);

export const getUserInfoRequestFailure = createAction(
  AuthActionTypes.GET_USER_INFO_FAILURE,
  props<{ payload: string }>()
);

export const editInfoRequest = createAction(
  AuthActionTypes.EDIT_INFO_REQUEST,
  props<{ payload: authInfo }>()
);

export const editInfoSuccess = createAction(AuthActionTypes.EDIT_INFO_SUCCESS);

export const editInfoFailure = createAction(
  AuthActionTypes.EDIT_INFO_FAILURE,

  props<{ payload: string }>()
);

export const resetUserInfo = createAction(AuthActionTypes.RESET_USER_INFO);
