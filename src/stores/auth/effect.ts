import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, switchMap } from 'rxjs';
import { UserService } from 'src/app/service/user.service';
import { AlertService } from 'src/app/services/alert.service';
import { LoginActionTypes } from '../login/actions';
import {
  AuthActionTypes,
  editInfoFailure,
  editInfoSuccess,
  getUserInfoRequestFailure,
  getUserInfoRequestSuccess,
  resetUserInfo,
} from './actions';

@Injectable()
export class AuthEffect {
  constructor(
    private actions: Actions,
    private userService: UserService,
    private alertService: AlertService
  ) {}

  getUserInfo$ = createEffect(() => {
    return this.actions.pipe(
      ofType(AuthActionTypes.GET_USER_INFO_REQUEST),
      switchMap((values: any) => {
        const { payload } = values;
        return this.userService.getUser(payload).pipe(
          map(user => {
            return getUserInfoRequestSuccess({ payload: user });
          }),
          catchError(err => of(getUserInfoRequestFailure(err)))
        );
      })
    );
  });

  resetUserInfo$ = createEffect(() => {
    return this.actions.pipe(
      ofType(LoginActionTypes.LOGOUT_REQUEST_SUCCESS),
      switchMap(() => {
        return of(resetUserInfo());
      })
    );
  });

  editInfoRequest$ = createEffect(() => {
    return this.actions.pipe(
      ofType(AuthActionTypes.EDIT_INFO_REQUEST),
      switchMap((values: any) => {
        const { payload } = values;
        return this.userService.updateAccountInfo(payload).pipe(
          map(() => {
            this.alertService.addAlert({
              severity: 'success',
              summary: '',
              detail: 'Your info is updated',
            });
            return editInfoSuccess();
          }),
          catchError(err => {
            this.alertService.addAlert({
              severity: 'error',
              summary: '',
              detail: err,
            });
            return of(editInfoFailure(err));
          })
        );
      })
    );
  });
}
