import { AuthActionTypes } from './actions';
import { authInitialState } from './state';

export default function authReducer(state = authInitialState, action: any) {
  const { type, payload } = action;

  console.log(type);

  switch (type) {
    case AuthActionTypes.GET_USER_INFO_REQUEST: {
      return { ...state, loading: false };
    }
    case AuthActionTypes.GET_USER_INFO_SUCCESS: {
      return { ...state, user: payload };
    }
    case AuthActionTypes.GET_USER_INFO_FAILURE: {
      return { ...state, loading: false };
    }
    case AuthActionTypes.RESET_USER_INFO:
      return { ...state, user: null };

    case AuthActionTypes.EDIT_INFO_REQUEST:
      return { ...state, loading: true };

    case AuthActionTypes.EDIT_INFO_SUCCESS:
      return { ...state, loading: false };

    case AuthActionTypes.EDIT_INFO_FAILURE:
      return { ...state, loading: false };

    default:
      return state;
  }
}
