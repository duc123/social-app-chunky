import { createFeatureSelector, createSelector } from '@ngrx/store';

import { authState } from './state';

const selectAuthState = createFeatureSelector<authState>('auth');

export const selectAuth = createSelector(
  selectAuthState,
  (state: authState) => state.user
);

export const selectLoading = createSelector(
  selectAuthState,
  (state: authState) => state.loading
);
