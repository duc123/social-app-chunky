import { userInterface } from 'src/interfaces/user.interface';

export interface authState {
  user: userInterface | null;
  loading: boolean;
}

export const authInitialState: authState = {
  user: null,
  loading: false,
};
