/* eslint-disable @ngrx/prefer-action-creator */
import { Action } from '@ngrx/store';

export enum LoginActionTypes {
  LOGIN_REQUEST = '[Login] request',
  LOGIN_REQUEST_SUCCESS = '[Login] request success',
  LOGIN_REQUEST_FAILURE = '[Login] request failure',
  LOGOUT_REQUEST = '[Logout] request',
  LOGOUT_REQUEST_SUCCESS = '[Logout] request success',
  LOGOUT_REQUEST_FAILURE = '[Logout] request failure',
}

export class LoginRequest implements Action {
  type = LoginActionTypes.LOGIN_REQUEST;
  constructor(public payload: { email: string; password: string }) {}
}

export class LoginRequestSuccess implements Action {
  type = LoginActionTypes.LOGIN_REQUEST_SUCCESS;
  constructor(public payload: string) {}
}

export class LoginRequestFailure implements Action {
  type = LoginActionTypes.LOGIN_REQUEST_FAILURE;
  constructor(public payload: string) {}
}

export class LogoutRequest implements Action {
  type = LoginActionTypes.LOGOUT_REQUEST;
  constructor() {}
}

export class LogoutRequestSuccess implements Action {
  type = LoginActionTypes.LOGOUT_REQUEST_SUCCESS;
  constructor() {}
}

export class LogoutRequestFailure implements Action {
  type = LoginActionTypes.LOGOUT_REQUEST_FAILURE;
  constructor(public payload: string) {}
}
