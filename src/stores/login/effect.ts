import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { LoginService } from 'src/app/services/login.service';
import { catchError, map, of, switchMap } from 'rxjs';
import {
  LoginActionTypes,
  LoginRequestFailure,
  LoginRequestSuccess,
  LogoutRequestFailure,
  LogoutRequestSuccess,
} from './actions';

// import { LoginService } from 'src/app/services/login.service';
// import {
//   ELoginRequestFailure,
//   ELoginRequestSuccess,
//   LoginActionTypes,
//   LogoutFailure,
//   LogoutSuccess,
// } from './actions';
// @Injectable()
// export class ELoginEffect {
//   constructor(private actions$: Actions, private loginService: LoginService) {}

//   eLogin$ = createEffect(() => {
//     return this.actions$.pipe(
//       ofType(LoginActionTypes.E_LOGIN_REQUEST),
//       switchMap((values: any) => {
//         const {
//           payload: { email, password },
//         } = values;
//         return this.loginService.loginEmployeeRequest(email, password).pipe(
//           map((user: any) => {
//             return new ELoginRequestSuccess(user.user.uid);
//           }),
//           catchError(err => of(new ELoginRequestFailure(err)))
//         );
//       })
//     );
//   });

//   logout$ = createEffect(() => {
//     return this.actions$.pipe(
//       ofType(LoginActionTypes.LOGOUT_REQUEST),
//       switchMap(() => {
//         return this.loginService.logout().pipe(
//           map(() => new LogoutSuccess()),
//           catchError(err => of(new LogoutFailure(err)))
//         );
//       })
//     );
//   });
// }
@Injectable()
export class LoginEffect {
  constructor(private loginService: LoginService, private actions: Actions) {}

  login$ = createEffect(() => {
    return this.actions.pipe(
      ofType(LoginActionTypes.LOGIN_REQUEST),
      switchMap((values: any) => {
        this.loginService.clearMsg();
        const {
          payload: { email, password },
        } = values;

        return this.loginService.login(email, password).pipe(
          map(user => new LoginRequestSuccess(user.user.uid)),
          catchError(err => {
            this.loginService.addMsg({
              severity: 'error',
              summary: '',
              detail: err,
            });

            return of(new LoginRequestFailure(err));
          })
        );
      })
    );
  });

  logout$ = createEffect(() => {
    return this.actions.pipe(
      ofType(LoginActionTypes.LOGOUT_REQUEST),
      switchMap(() => {
        return this.loginService.logout().pipe(
          map(() => new LogoutRequestSuccess()),
          catchError(err => of(new LogoutRequestFailure(err)))
        );
      })
    );
  });
}
