import { LoginActionTypes } from './actions';
import { loginInitialState, loginState } from './state';

const saveUid = (uid: string) => {
  localStorage.setItem('uid', JSON.stringify(uid));
};

const removeUid = () => {
  localStorage.removeItem('uid');
};

export default function loginReducer(
  state: loginState = loginInitialState,
  action: any
) {
  const { type, payload } = action;

  switch (type) {
    case LoginActionTypes.LOGIN_REQUEST: {
      return { ...state, loading: true };
    }
    case LoginActionTypes.LOGIN_REQUEST_SUCCESS: {
      saveUid(payload);
      return { ...state, loading: false, uid: payload };
    }
    case LoginActionTypes.LOGIN_REQUEST_FAILURE: {
      return { ...state, loading: false, uid: '' };
    }
    case LoginActionTypes.LOGOUT_REQUEST: {
      return { ...state, loading: true };
    }
    case LoginActionTypes.LOGOUT_REQUEST_SUCCESS: {
      removeUid();
      return { ...state, loading: false, uid: '' };
    }
    case LoginActionTypes.LOGOUT_REQUEST_FAILURE: {
      removeUid();
      return { ...state, loading: false, uid: '' };
    }
    default:
      return state;
  }
}
