import { createSelector, createFeatureSelector } from '@ngrx/store';

import { loginState } from './state';

const selectLoginState = createFeatureSelector<loginState>('login');

export const selectLoginLoading = createSelector(
  selectLoginState,
  (state: loginState) => state.loading
);

export const selectLoginUid = createSelector(
  selectLoginState,
  (state: loginState) => state.uid
);
