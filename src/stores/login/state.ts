export interface loginState {
  uid: string;
  error: string;
  loading: boolean;
}

const uidStore = localStorage.getItem('uid');

const uid = uidStore ? JSON.parse(uidStore) : '';

export const loginInitialState: loginState = {
  uid,
  error: '',
  loading: false,
};
