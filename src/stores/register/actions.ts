import { createAction, props } from '@ngrx/store';
import { registerData } from 'src/interfaces/register.data';

export enum registerActionTypes {
  REGISTER_REQUEST = '[Register] request',
  REGISTER_REQUEST_SUCCESS = '[Register] request success',
  REGISTER_REQUEST_FAILURE = '[Register] request failure',
  REGISTER_SET_DATA_REQUEST = '[Register] set data request',
  REGISTER_SET_DATA_SUCCESS = '[Register] set data success',
  REGISTER_SET_DATA_FAILURE = '[Register] set data failure',
  REGISTER_HIDE_ERROR = '[Register] hide error',
}

export const registerRequest = createAction(
  registerActionTypes.REGISTER_REQUEST,
  props<{ payload: registerData }>()
);

export const registerRequestSuccess = createAction(
  registerActionTypes.REGISTER_REQUEST_SUCCESS,
  props<{ payload: { uid: string; data: registerData } }>()
);

export const registerRequestFailure = createAction(
  registerActionTypes.REGISTER_REQUEST_FAILURE,
  props<{ payload: string }>()
);

export const registerSetDataRequest = createAction(
  registerActionTypes.REGISTER_SET_DATA_REQUEST,
  props<{ payload: { uid: string; data: registerData } }>()
);

export const registerSetDataSuccess = createAction(
  registerActionTypes.REGISTER_SET_DATA_SUCCESS,
  props<{ payload: string }>()
);

export const registerSetDataFailure = createAction(
  registerActionTypes.REGISTER_SET_DATA_FAILURE,
  props<{ payload: string }>()
);

export const registerHideError = createAction(
  registerActionTypes.REGISTER_HIDE_ERROR
);
