import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, switchMap } from 'rxjs';
import { RegisterService } from 'src/app/services/register.service';
import { LoginRequestSuccess } from '../login/actions';
import {
  registerActionTypes,
  registerRequestFailure,
  registerRequestSuccess,
  registerSetDataFailure,
  registerSetDataRequest,
  registerSetDataSuccess,
} from './actions';

@Injectable()
export class RegisterEffect {
  constructor(
    private actions: Actions,
    private registerService: RegisterService
  ) {}

  register$ = createEffect(() => {
    return this.actions.pipe(
      ofType(registerActionTypes.REGISTER_REQUEST),
      switchMap((values: any) => {
        const {
          payload: { username, email, password },
        } = values;
        const data = { username, email, password };
        this.registerService.clearMsg();

        return this.registerService.register(data).pipe(
          map((res: any) => {
            const { user } = res;
            return registerRequestSuccess({ payload: { uid: user.uid, data } });
          }),
          catchError(err => {
            this.registerService.addMsg({
              severity: 'error',
              summary: 'Login error',
              detail: err,
            });
            return of(registerRequestFailure(err));
          })
        );
      })
    );
  });

  registerSuccess$ = createEffect(() => {
    return this.actions.pipe(
      ofType(registerActionTypes.REGISTER_REQUEST_SUCCESS),
      switchMap((values: any) => {
        const { payload } = values;
        return of(registerSetDataRequest({ payload }));
      })
    );
  });

  registerSetData$ = createEffect(() => {
    return this.actions.pipe(
      ofType(registerActionTypes.REGISTER_SET_DATA_REQUEST),
      switchMap((values: any) => {
        const {
          payload: { uid, data },
        } = values;
        return this.registerService
          .registerSetData(uid, data.username, data.email)
          .pipe(
            map((res: any) => {
              return registerSetDataSuccess({ payload: uid });
            }),
            catchError(err => {
              this.registerService.addMsg({
                severity: 'error',
                summary: 'Login error',
                detail: err,
              });
              return of(registerSetDataFailure({ payload: err }));
            })
          );
      })
    );
  });

  registerSetDataSuccess$ = createEffect(() => {
    return this.actions.pipe(
      ofType(registerActionTypes.REGISTER_SET_DATA_SUCCESS),
      switchMap((values: any) => {
        const { payload } = values;
        console.log(values);

        return of(new LoginRequestSuccess(payload));
      })
    );
  });
}
