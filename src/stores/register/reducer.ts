import { registerActionTypes } from './actions';
import { registerInitialState, registerState } from './state';

export default function registerReducer(
  state: registerState = registerInitialState,
  action: any
) {
  const { type, payload } = action;

  switch (type) {
    case registerActionTypes.REGISTER_REQUEST: {
      return { ...state, loading: true };
    }
    case registerActionTypes.REGISTER_REQUEST_SUCCESS: {
      return { ...state, loading: false, error: '' };
    }
    case registerActionTypes.REGISTER_REQUEST_FAILURE: {
      return { ...state, loading: false };
    }
    case registerActionTypes.REGISTER_SET_DATA_REQUEST: {
      return { ...state, loading: true };
    }
    case registerActionTypes.REGISTER_SET_DATA_SUCCESS: {
      return { ...state, loading: false, error: '' };
    }
    case registerActionTypes.REGISTER_SET_DATA_FAILURE: {
      return { ...state, loading: false, error: payload };
    }
    case registerActionTypes.REGISTER_HIDE_ERROR: {
      return { ...state, error: '' };
    }
    default:
      return state;
  }
}
