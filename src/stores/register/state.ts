export interface registerState {
  error: string;
  loading: boolean;
}

export const registerInitialState: registerState = {
  error: '',
  loading: false,
};
